
public class HD extends Pecas{
	
	public HD(IComputador itens) {
		super(itens);
	}
	@Override
	public String descricao() {
		return itens.descricao() + ", " + "HD";
	}
	@Override
	public double valor() {
		return itens.valor() + 230.00;
	}
	@Override
	public String toString() {
		return "R$ " + valor() + ": " + descricao();
	}
}
