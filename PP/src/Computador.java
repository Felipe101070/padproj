
public class Computador implements IComputador {
	
	@Override
	public double valor() {
		return 500.00;
	}

	@Override
	public String descricao() {
		return "Gabinete";
	}
	@Override
	public String toString() {
		return "R$" + valor() + ": " + descricao();
	}

}
