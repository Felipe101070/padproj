
public class Dirigir {
	private static Dirigir instance[];
	private static int i;
	private static int MAXIMO = 3;
		
	private Dirigir() {
	}
	public static Dirigir getInstance() {
		if(instance == null) {
			instance = new Dirigir[MAXIMO];
			criarInstancias();
		}
		i=i+1;
		if(i==MAXIMO) {
			i=0;
		}
		return instance[i];
	}
	
	private static void criarInstancias() {
		for(int j = 0; j < MAXIMO ; j++) {
			instance[j] = new Dirigir();
		}
	}
	public static void Acelerar() {
		System.out.println("Veiculo aumenta velocidade...");
	}
	public static void Frear() {
		System.out.println("Veiculo diminui velocidade...");
	}
	public static void VirarDireita() {
		System.out.println("Veiculo vira para Direita...");
	}
	public static void VirarEsquerda() {
		System.out.println("Veiculo vira para Esquerda...");
	}
	public static void DarRe() {
		System.out.println("Veiculo da r�...");
	}
}
