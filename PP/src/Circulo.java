
public class Circulo implements IFiguraBiDimensional {
	
	private double raio;
	
	private boolean condicaoExistencia(int r) {
		return r > 0;
	}
	public Circulo() {
		this.raio = 1;
	}
	public Circulo(int raio) {
		if(!condicaoExistencia(raio)) {
			throw new RuntimeException("Impossivel construir Circulo. ");
		}
		this.raio = raio;
	}
	public double getRaio() {
		return raio;
	}
	public void setRaio(int raio) {
		if(condicaoExistencia(raio)) {
			this.raio = raio;
		}
	}
	@Override
	public double perimetro() {
		return 2 * Math.PI * raio;
	}

	@Override
	public double area() {
		return 0;
	}
	@Override
	public String toString() {
		return "(" + this.raio + ", diametro: " + this.raio * 2 + ")";
	}


}
