
public class Tarefa implements Runnable{
	private int numero;
	
	public Tarefa(int numero) {
		this.numero = numero;
	}

	@Override
	public void run() {
		System.out.println(numero + ":" + Singleton.getInstance());
	}

}
