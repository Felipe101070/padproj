
public class RAM extends Pecas{
	
	public RAM(IComputador itens) {
		super(itens);
	}
	@Override
	public String descricao() {
		return itens.descricao() + ", " + "8G RAM";
	}
	@Override
	public double valor() {
		return itens.valor() + 370.00;
	}
	@Override
	public String toString() {
		return "R$ " + valor() + ": " + descricao();
	}
}
