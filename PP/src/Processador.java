
public class Processador extends Pecas{

	public Processador(IComputador itens) {
		super(itens);
	}

	@Override
	public String descricao() {
		return itens.descricao() + ", " + "processador";
	}
	@Override
	public double valor() {
		return itens.valor() + 500.00;
	}
	@Override
	public String toString() {
		return "R$ " + valor() + ": " + descricao();
	}
}
