import javax.swing.JOptionPane;

public class Adapter implements ITarget {

	private Adaptee2 adaptee;
	
	public Adapter() {
		adaptee = new Adaptee2();
	}
	
	@Override
	public void exibirMensagem(String msg) {
		String str[] = msg.split(":");
		int tipo = JOptionPane.INFORMATION_MESSAGE;
		
		if(str[0].equals("ERR")) {
			tipo = JOptionPane.ERROR_MESSAGE;
		}
		if(str[0].equals("WARN")) {
			tipo = JOptionPane.WARNING_MESSAGE;
		}
		String mensagem = str.length > 1 ? str[1] : str[0];
		
		adaptee.mostrarMensagem(mensagem, tipo);	
	}
	

}
