
public class Fonte extends Pecas{
	
	public Fonte(IComputador itens) {
		super(itens);
	}
	@Override
	public String descricao() {
		return itens.descricao() + ", " + "Fonte de alimentacao";
	}
	@Override
	public double valor() {
		return itens.valor() + 310.00;
	}
	@Override
	public String toString() {
		return "R$ " + valor() + ": " + descricao();
	}

}
