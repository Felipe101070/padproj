
public class PlacaMae extends Pecas{
	
	public PlacaMae(IComputador itens) {
		super(itens);
	}
	@Override
	public String descricao() {
		return itens.descricao() + ", " + "Placa Mae";
	}
	@Override
	public double valor() {
		return itens.valor() + 460.00;
	}
	@Override
	public String toString() {
		return "R$ " + valor() + ": " + descricao();
	}

}
