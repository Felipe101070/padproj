
public class ConstrutorFiguraBiDimensionais implements IConstrucaoFiguraBiDimensional {

	@Override
	public IFiguraBiDimensional criar(String nomeFigura) {
		if(nomeFigura.equals("triangulo")) {
			return new Triangulo(1);
		}
		if(nomeFigura.equals("retangulo")) {
			return new Retangulo();
		}
		if(nomeFigura.equals("circulo")) {
			return new Circulo();
		}
		if(nomeFigura.equals("quadrado")) {
			return new Quadrado();
		}

		return null;
	}

}
