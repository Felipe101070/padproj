
public class Corolla extends Carro{
	public Corolla(String type, String carMaker, double kilometerL) {
		setType(type);
		setCarMaker(carMaker);
		setKilometerL(kilometerL);
	}
	public Corolla(String carMaker, double kilometerL) {
		this.carMaker = carMaker;
		this.kilometerL = kilometerL;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setCarMaker(String carMaker) {
		this.carMaker = carMaker;
	}
	public void setKilometerL(double kilometerL) {
		this.kilometerL = kilometerL;
	}
	public String getType() {
		return type;
	}
	public String getCarMaker() {
		return carMaker;
	}
	public double getKilometerL() {
		return kilometerL;
	}

}
