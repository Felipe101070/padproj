
public class FactoryCarro {
	public Carro getCarro(String type, String carMaker, double kilometerL) {
		if(type.equals("Hatch")) {
			return new HB20(carMaker, kilometerL);
		}
		if(type.equals("Sedan")) {
			return new Corolla(carMaker, kilometerL);
		}
		if(type.equals("Picape")) {
			return new Toro(carMaker, kilometerL);
		}
		if(type.equals("Minivan")) {
			return new Viloran(carMaker, kilometerL);
		}
		return null;
	}
}
